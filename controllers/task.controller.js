const { Request, Response } = require("express");

const taskService = require("../services/task.service");

const taskController = {
        /**
         * GetAll
         * @param {Request} req
         * @param {Response} res
         */
        getAll : (req, res) => {

            const taskList = taskService.getAll();
            return res.status(200).json(taskList);

            return res.sendStatus(501); //En envoie juste un statusCode sans donnée
            //ou
            return res.status().json(); //En envoie un status, puis ensuite, un json
        },

        /**
         * GetById
         * @param {Request} req
         * @param {Response} res
         */
        getById : (req, res) => {
            const id = parseInt(req.params.id);
            //const id = +req.params.id;

            const task = taskService.getById(id);
            if(task){
                return res.status(200).json(task);
            }
            return res.status(404).json({ code : 404, message : `La tâche avec l'id ${id} n'existe pas`});

            return res.sendStatus(501);
        },

        /**
         * Create
         * @param {Request} req
         * @param {Response} res
         */
        create : (req, res) => {
            const taskToAdd = req.body;

            // Fausse verif
            // TODO : Remplacer par un middleware (yup)
            if(!taskToAdd.name || !taskToAdd.description) {
                return res.status(400).json({ code : 400, message : "Le champs name et le champs description sont requis"});
            }

            const task = taskService.create(taskToAdd);
            //pour respecter le principe rest
            //on envoie la requête à faire pour accéder à la ressources, permettant ainsi de la stocker pour la faire plus tard
            res.location('/api/task/'+task.id);
            //On renvoie l'objte créé
            return res.status(201).json(task);

            return res.sendStatus(501);
        },

        /**
         * Update
         * @param {Request} req
         * @param {Response} res
         */
        update : (req, res) => {
            return res.sendStatus(501);
        },

        /**
         * Delete
         * @param {Request} req
         * @param {Response} res
         */
        delete : (req, res) => {
            return res.sendStatus(501);
        }
}

module.exports = taskController;