const taskRouter = require("express").Router();

const taskController = require("../controllers/task.controller");

//get    /api/task/       -> getAll
//post   /api/task/       -> create
taskRouter.route("/")
    .get(taskController.getAll)
    .post(taskController.create)

//get    /api/task/12     -> getById
//put    /api/task/12     -> update
//delete /api/task/12     -> delete 
taskRouter.route("/:id")
    .get(taskController.getById)
    .put(taskController.update)
    .delete(taskController.delete)

module.exports = taskRouter;