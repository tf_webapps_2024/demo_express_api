// TODO : Remplacer ça par des vraies données en DB
const tasks = [
    { id : 1, name : "Faire la vaisselle", description : "Y'a beaucoup là", creator : "Aude", slave : "Aurélien", done : false},
    { id : 2, name : "Me remplacer jeudi 14", description : "J'ai une feature à rajouter à mon projet", creator : "Aurélien", slave : "Aude", done : true},
]

const taskService = {
    getAll : () => {
        return tasks;
    },

    getById : (id) => {
        // for(const task of tasks) {
        //     if(task.id === id){
        //         return task
        //     }
        // }
        // return undefined;
        return tasks.find(task => task.id === id);
    },

    create : ( taskToAdd ) => {
        //création de l'id
        //tasks.map(task => task.id) // Transforme notre tableau de task en tableau avec juste les id // [1, 2] 
        //...tasks.map(task => task.id) // 1,2 //les id //destructuring
        taskToAdd.id = Math.max( ...tasks.map(task => task.id) ) + 1;

        tasks.push(taskToAdd);
        return taskToAdd;

    },

    update : () => {},

    delete : () => {}



}

module.exports = taskService;